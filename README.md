# Metadaten for HIL and experiments

The project will gather international efforts to define a metadata scheme for high intensity laser (HIL) systems and associated experiments. 
HIL systems achieve power in the petawatt range for tens to hundreds of femptoseconds.The research fields are: Acceleration of ions and electrons, generation of secondary radiation and higher harmonics, tests of QED and much more. The list of relevant publications is coming soon. If you already have ideas and drafts in this field, we would be pleased if you link them here, or include them in the scheme. Also, if you come from related research areas, such as plasma research or surface microstructuring, we would be happy if you refer us to your metadata schemes and advise us.

As a starting point for discussions, I post a MindMap about topics for which we need to define metadata schemas, or use existing ones. The mindmap was created with the great tool [FreeMind](http://freemind.sourceforge.net/wiki/index.php/Download). I also post a first very delitant draft of an ontology (the .owl file). This can be shown and  edited e.g. with [Protegee](https://protege.stanford.edu/). 

## Popular Metadata Standards

### openPMD

The Open Standard for Particle-Mesh Data (openPMD) provides naming and attribute conventions that allow for the exchange of particle and mesh-based data from scientific simulations and experiments.
Its primary goals are to define a *minimal set/kernel of meta information* that allows to share and exchange data to achieve
- portability between various applications and differing algorithms
- a unified open-access description for scientific data (publishing and archiving)
- a unified description for post-processing, visualization, and analysis.

openPMD suits for any kind of hierarchical, self-describing data format, such as, but not limited to
- ADIOS1 (BP3)
- ADIOS2 (BP4)
- HDF5
- JSON
- XML

Links:
- **Homepage/Intro:** [www.openPMD.org](https://www.openPMD.org)
- **GitHub:** [github.com/openPMD](https://github.com/openPMD)
- **Catalog of Projects/Software:** [github.com/openPMD/openPMD-projects](https://github.com/openPMD/openPMD-projects)
  - Example: [openPMD-CCD](https://github.com/openPMD/openPMD-CCD)
- **Standard Documents**: [github.com/openPMD/openPMD-standard](https://github.com/openPMD/openPMD-standard) (CC-BY-4.0) ([DOI:10.5281/zenodo.591699](https://doi.org/10.5281/zenodo.591699))
- **Recent intro talk:**
  - [DOI:10.5281/zenodo.5905053](https://doi.org/10.5281/zenodo.5905053)
  - [Video](https://www.youtube.com/watch?v=p_Jqc5dCpmI&list=PLxDjU8Z0i6V9KVSofoZWCUI1F3BLXH8GF&index=6) (ping me if offline, will re-upload then)
- **Contacts:** email Axel Huebl (prev. HZDR, now LBNL) <axelhuebl@lbl.gov>, [Gitter](https://gitter.im/openPMD/Lobby), [Slack](https://openpmd.slack.com), [GitHub Issues](https://github.com/openPMD)
